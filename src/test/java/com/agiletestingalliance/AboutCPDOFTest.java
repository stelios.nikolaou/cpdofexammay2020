package com.agiletestingalliance;

import static org.junit.Assert.*;
import org.junit.Test;

public class AboutCPDOFTest {

  @Test
  public void durTest() throws Exception {
    assertTrue("About",new AboutCPDOF().desc().contains("CP-DOF certification program"));
  }

}

