package com.agiletestingalliance;

import static org.junit.Assert.*;
import org.junit.Test;

public class UsefulnessTest {

  @Test
  public void descTest() throws Exception {
    assertTrue("Description",new Usefulness().desc().contains("DevOps is about transformation"));
  }

}
