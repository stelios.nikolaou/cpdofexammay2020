package com.agiletestingalliance;

import static org.junit.Assert.*;
import org.junit.Test;

public class TestClassTest {

  @Test
  public void gstrTest() throws Exception {
    TestClass test = new TestClass("Test");
    assertEquals("TestClass","Test",test.gstr());
  }
}
