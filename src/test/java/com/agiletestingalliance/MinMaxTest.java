package com.agiletestingalliance;

import static org.junit.Assert.*;
import org.junit.Test;

public class MinMaxTest {

  @Test
  public void maxTest1() throws Exception {
    assertEquals("Max", 9, new MinMax().max(9,2));
  }

  @Test
  public void maxTest2() throws Exception {
    assertEquals("Max", 12, new MinMax().max(6,12));
  }

  @Test
  public void barTest1() throws Exception {
    assertEquals("Bar","Something", new MinMax().bar("Something"));
  }

  @Test
  public void barTestEmpty() throws Exception {
    assertEquals("Bar","", new MinMax().bar(""));
  }

}
